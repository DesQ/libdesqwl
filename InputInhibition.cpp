/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This file wa originally written as a part of PaperDE.
	* https://gitlab.com/cubocore/paperde)
	* Suitabe changes have been made to suit the needs of DesQ
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <QDebug>
#include <QWindow>

#include <wayland-client.h>

#include "InputInhibition.hpp"
#include "WlGlobal.hpp"

#include "wayland-wlr-input-inhibit-unstable-v1-client-protocol.h"

/*
	*
	* DesQ::Wayland::InputInhibitManager
	*
*/

DesQ::Wayland::InputInhibitManager::InputInhibitManager( zwlr_input_inhibit_manager_v1 *iimgr ) : QObject() {

	mObj = iimgr;
};

DesQ::Wayland::InputInhibitManager::~InputInhibitManager() {

	zwlr_input_inhibit_manager_v1_destroy( mObj );
};

DesQ::Wayland::InputInhibitor* DesQ::Wayland::InputInhibitManager::getInputInhibitor() {

	return new DesQ::Wayland::InputInhibitor( zwlr_input_inhibit_manager_v1_get_inhibitor( mObj ) );
};

DesQ::Wayland::InputInhibitManager::operator zwlr_input_inhibit_manager_v1 *() {

	return mObj;
};

DesQ::Wayland::InputInhibitManager::operator zwlr_input_inhibit_manager_v1 *() const {

	return mObj;
};

/*
	*
	* DesQ::Wayland::InputInhibitor
	*
*/

DesQ::Wayland::InputInhibitor::InputInhibitor( zwlr_input_inhibitor_v1 *ii ) : QObject() {

	mObj = ii;
};

DesQ::Wayland::InputInhibitor::~InputInhibitor() {

	zwlr_input_inhibitor_v1_destroy( mObj );
};

void DesQ::Wayland::InputInhibitor::destroy() {

	zwlr_input_inhibitor_v1_destroy( mObj );
};

DesQ::Wayland::InputInhibitor::operator zwlr_input_inhibitor_v1 *() {

	return mObj;
};

DesQ::Wayland::InputInhibitor::operator zwlr_input_inhibitor_v1 *() const {

	return mObj;
};
