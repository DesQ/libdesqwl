/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This file wa originally written as a part of PaperDE.
	* https://gitlab.com/cubocore/paperde)
	* Suitabe changes have been made to suit the needs of DesQ
	*

	*
	* This file was a part of IonShell (https://gitlab.com/cubocore/ion/ionde)
	* Suitable modifications have been done to meet the needs of DesQ.
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <QGuiApplication>
#include <QWindow>
#include <QScreen>

#include <qpa/qplatformnativeinterface.h>
#include <wayland-client.h>

#include "XdgPositioner.hpp"

DesQ::Wayland::XdgPositioner::XdgPositioner( const QSize& initialSize = QSize(), const QRect& anchor = QRect() ) {

	mInitialSize = initialSize;
	mAnchorEdge = anchor;
};

DesQ::Wayland::XdgPositioner::XdgPositioner( const XdgPositioner& other ) {

	mObj = static_cast<xdg_positioner *>( &other );
};

DesQ::Wayland::XdgPositioner::~XdgPositioner() {

	xdg_positioner_destroy( mObj );
	mObj = nullptr;
};

Anchors DesQ::Wayland::XdgPositioner::anchorEdge() const {

	return mAnchorEdge;
};

void DesQ::Wayland::XdgPositioner::setAnchorEdge( Anchors edge ) {

	mAnchorEdge = edge;
	xdg_positioner_set_anchor( mObj, edge );
};

Gravity DesQ::Wayland::XdgPositioner::gravity() const {

	return mGravity;
};

void DesQ::Wayland::XdgPositioner::setGravity( Gravity edge ) {

	mGravity = edge;
	xdg_positioner_set_gravity( mObj, edge );
};

QRect DesQ::Wayland::XdgPositioner::anchorRect() const {

	return mAnchorRect;
};

void DesQ::Wayland::XdgPositioner::setAnchorRect( const QRect& rect ) {

	mAnchorRect = rect;
	xdg_positioner_set_anchor_rect( mObj, rect.x(), rect.y(), rect.width(), rect.height() );
};

QSize DesQ::Wayland::XdgPositioner::initialSize() const {

	return mInitialSize;
};

void DesQ::Wayland::XdgPositioner::setInitialSize( const QSize& size ) {

	mInitialSize = size;
	xdg_positioner_set_size( mObj, size.width(), size.height() );
};

Constraints DesQ::Wayland::XdgPositioner::constraints() const {

	return mConstraints;
};

void DesQ::Wayland::XdgPositioner::setConstraints( Constraints constraints ) {

	mConstraints = constraints;
	xdg_positioner_set_constraint_adjustment( constraints );
};

QPoint DesQ::Wayland::XdgPositioner::anchorOffset() const {

	return mAnchorOffset;
};

void DesQ::Wayland::XdgPositioner::setAnchorOffset( const QPoint& offset ) {

	mAnchorOffset = offset;
	xdg_positioner_set_offset( mObj, offset.x(), offset.y() );
};

void DesQ::Wayland::XdgPositioner::setReactive() {

	xdg_positioner_set_reactive( mObj );
};
