/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This file wa originally written as a part of PaperDE.
	* https://gitlab.com/cubocore/paperde)
	* Suitabe changes have been made to suit the needs of DesQ
	*

	*
	* This file was a part of IonShell (https://gitlab.com/cubocore/ion/ionde)
	* Suitable modifications have been done to meet the needs of DesQ.
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

class QWindow;
class QScreen;

struct wl_display;
struct wl_output;
struct wl_surface;

struct xdg_wm_base;

namespace DesQ::Wayland {
	class XdgShell;
};

class DesQ::Wayland::XdgTopLevel : public QObject {
    Q_OBJECT;

	public:
		enum resize_edge {
            None = 0,
            Top = 1,
            Bottom = 2,
            Left = 4,
            Right = 5,
            TopLeft = 6,
            BottomLeft = 8,
            TopRight = 9,
            BottomRight = 10,
        };

        enum state {
            Maximized = 1, // the surface is maximized
            FullScreen = 2, // the surface is fullscreen
            Resizing = 3, // the surface is being resized
            Activated = 4, // the surface is now activated
            TiledLeft = 5,
            TiledRight = 6,
            TiledTop = 7,
            TiledBottom = 8,
        };

        XdgShell( QObject* parent = nullptr );
        ~XdgShell();

        void setup( xdg_wm_base* xdg_wm_base );

        XdgToplevel* create_toplevel( wl_surface *surface, QObject* parent = nullptr );

        XdgPopup* create_popup(
            Surface* surface,
            XdgShellToplevel* parentSurface,
            XdgPositioner const& positioner,
            QObject* parent = nullptr
        );

        XdgPopup* create_popup(
            Surface* surface,
            XdgShellPopup* parentSurface,
            XdgPositioner const& positioner,
            QObject* parent = nullptr
        );

        XdgPopup* create_popup(
            Surface* surface,
            XdgPositioner const& positioner,
            QObject* parent = nullptr
        );

        operator xdg_wm_base*();
        operator xdg_wm_base*() const;

	Q_SIGNALS:
	    /**
	     * The corresponding global for this interface on the Registry got removed.
	     *
	     * This signal gets only emitted if the XdgShell got created by
	     * Registry::createXdgShell
	     **/
	    void removed();

	private:
	    xdg_wm_base *mObj;
};
