# libdesqwl

The DesQ library to handle Wayland protocols to be used across the DesQ project.

### Notes for compiling (Qt5) - linux:

* Download the sources
  * Git: `git clone https://gitlab.com/DesQ/libdesqwl.git libdesqwl`
* Enter `libdesq`
* Open the terminal and type: `qmake -qt5 && make`
* To install, as root type: `make install`

### Dependencies:
* Qt5 (qtbase5-dev, qtbase5-dev-tools)
* wayland (libwayland-dev, wayland-protocols)
* wlroots (Preferably compiled from git)
* wayfire (Preferably compiled from git)

## My System Info
* OS:				Debian Sid
* Qt:				Qt5 5.12.5
* wayland:          1.18.0-2~exp1.1
* wlroots:          0.11.0-379835c4
* wayfire:          0.5.0-4d3d6c33

### Known Bugs
* Please test and let me know

### Upcoming
* Any other feature you request for... :)
