/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This file wa originally written as a part of PaperDE.
	* https://gitlab.com/cubocore/paperde)
	* Suitabe changes have been made to suit the needs of DesQ
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <QDebug>
#include <QWindow>

#include <wayland-client.h>

#include "WlGlobal.hpp"
#include "Registry.hpp"
#include "LayerShell.hpp"

#include "wayland-wlr-layer-shell-unstable-v1-client-protocol.h"

using namespace DesQ::Wayland;

/**
 * LayerShell Implementation
 */

LayerShell::LayerShell( zwlr_layer_shell_v1 *lShell ) {

	/* This is fully developed in wl_registry's hanbdle_global */
	mObj = lShell;
};

LayerShell::~LayerShell() {

	zwlr_layer_shell_v1_destroy( mObj );
};

LayerSurface* LayerShell::getLayerSurface( QWindow *window, wl_output *output, LayerShell::LayerType layer, const QString &lyrNs ) {

	wl_surface *surface = DesQ::Wayland::getWlSurface( window );
	/* We need a valid wl_surface */
	if ( not surface )
		return nullptr;

	/* We don't care if it's nullptr. Our compositor wll take case of this */
	if ( not output )
		output = DesQ::Wayland::getWlOutput( window->screen() );

	zwlr_layer_surface_v1 *lyr_surf = zwlr_layer_shell_v1_get_layer_surface( mObj, surface, output, layer, lyrNs.toUtf8().constData() );

	LayerSurface *lyrSurface = new LayerSurface( window, lyr_surf );

	return lyrSurface;
};

LayerShell::operator zwlr_layer_shell_v1 *() {

	return mObj;
};

LayerShell::operator zwlr_layer_shell_v1 *() const {

	return mObj;
};

/**
 * LayerSurface Implementation
 */

void LayerSurface::configureCallback( void *data, struct ::zwlr_layer_surface_v1 *object, uint32_t serial, uint32_t width, uint32_t height ) {

	auto ls = reinterpret_cast<LayerSurface *>( data );
	ls->configureSurface( serial, width, height );
};

void LayerSurface::closedCallback( void *data, struct ::zwlr_layer_surface_v1 *object ) {

	auto ls = reinterpret_cast<LayerSurface *>( data );
	ls->closeSurface();
};

const struct zwlr_layer_surface_v1_listener DesQ::Wayland::LayerSurface::mLyrSurfListener = {
    configureCallback,
    closedCallback,
};

LayerSurface::LayerSurface( QWindow *window, zwlr_layer_surface_v1 *surf ) {

	mWindow = window;
	mObj = surf;

	zwlr_layer_surface_v1_add_listener( surf, &mLyrSurfListener, this );
	wl_display_roundtrip( DesQ::Wayland::getWlDisplay() );
};

LayerSurface::~LayerSurface() {

	zwlr_layer_surface_v1_destroy( mObj );
};

void LayerSurface::apply() {

	zwlr_layer_surface_v1_set_anchor( mObj, static_cast<uint32_t>( m_anchors ) );
	zwlr_layer_surface_v1_set_exclusive_zone( mObj, m_exclusiveZone );
	zwlr_layer_surface_v1_set_keyboard_interactivity( mObj, m_keyboardInteractivity );

	if ( m_surfaceSize.isValid() )
		zwlr_layer_surface_v1_set_size( mObj, m_surfaceSize.width(), m_surfaceSize.height() );

	else
		zwlr_layer_surface_v1_set_size( mObj, mWindow->width(), mWindow->height() );

	zwlr_layer_surface_v1_set_margin( mObj, m_margins.top(), m_margins.right(), m_margins.bottom(), m_margins.left() );

	wl_surface_commit( DesQ::Wayland::getWlSurface( mWindow ) );
	wl_display_roundtrip( DesQ::Wayland::getWlDisplay() );
};

void LayerSurface::setSurfaceSize( const QSize &surfaceSize ) {

	m_surfaceSize = surfaceSize;
	zwlr_layer_surface_v1_set_size( mObj, m_surfaceSize.width(), m_surfaceSize.height() );
	wl_surface_commit( DesQ::Wayland::getWlSurface( mWindow ) );
};

void LayerSurface::setAnchors( const SurfaceAnchors &anchors ) {

	m_anchors = anchors;
	zwlr_layer_surface_v1_set_anchor( mObj, (uint32_t)m_anchors );
};

void LayerSurface::setExclusiveZone( int exclusiveZone ) {

	m_exclusiveZone = exclusiveZone;
	zwlr_layer_surface_v1_set_exclusive_zone( mObj, m_exclusiveZone );
};

void LayerSurface::setMargins( const QMargins &margins ) {

	m_margins = margins;
	zwlr_layer_surface_v1_set_margin( mObj, m_margins.top(), m_margins.right(), m_margins.bottom(), m_margins.left() );
};

void LayerSurface::setKeyboardInteractivity( LayerSurface::FocusType focusType ) {

	m_keyboardInteractivity = ( uint )focusType;
	zwlr_layer_surface_v1_set_keyboard_interactivity( mObj, m_keyboardInteractivity );
};

void LayerSurface::setLayer( DesQ::Wayland::LayerShell::LayerType type ) {

	m_lyrType = type;
	zwlr_layer_surface_v1_set_layer( mObj, ( uint )m_lyrType );
};

void LayerSurface::getPopup( QWindow *popupWindow ) {

	// getPopup( reg, popupWindow );
	// wl_surface *wl_surface_popup = DesQ::Wayland::getWlSurface( popupWindow );
};

LayerSurface::operator zwlr_layer_surface_v1*() {

	return mObj;
};

LayerSurface::operator zwlr_layer_surface_v1*() const {

	return mObj;
};

void LayerSurface::configureSurface( uint32_t serial, uint32_t width, uint32_t height ) {

	mWindow->resize( width, height );
	zwlr_layer_surface_v1_ack_configure( mObj, serial );
};

void LayerSurface::closeSurface() {

	mWindow->close();
};
