/*
	*
	* This file is a part of DesQ.
	* DesQSession is a Session manager for DesQ Shell
    * Copyright 2020 Marcus Britanicus
	*

	*
	* This file is derived from QSingleApplication, originally written
	* as a part of Qt Solutions. For license read DesQApplication.cpp
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include <QApplication>
#include <QLockFile>

#if defined(qApp)
#undef qApp
#endif

namespace DesQ {
    namespace Wayland {
        class Application;
        class Registry;
    };
};

#define qApp (static_cast<DesQ::Wayland::Application *>(QCoreApplication::instance()))

class DesQIpcServer;

class DesQ::Wayland::Application : public QApplication {
    Q_OBJECT;

	public:
		Application( int &argc, char **argv, bool GUIenabled = true );
		Application( const QString &id, int &argc, char **argv );

		~Application();

		bool isRunning();
		QString id() const;

		void setActivationWindow( QWidget* aw, bool activateOnMessage = true );
		QWidget* activationWindow() const;

        DesQ::Wayland::Registry* waylandRegistry();
        DesQIpcServer *ipcServer();

	public Q_SLOTS:
		bool sendMessage( const QString &message );
		void activateWindow();
		void disconnect();

	Q_SIGNALS:
		void messageReceived( const QString &message );

	private:
        QLockFile *lockFile = nullptr;
		DesQIpcServer *mIpcServer = nullptr;

        QString mSocketName;
        QString mAppId;

		QWidget *actWin;
        DesQ::Wayland::Registry *registry = nullptr;
};
