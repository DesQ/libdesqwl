/*
	*
	* This file is a part of DesQ.
	* DesQSession is a Session manager for DesQ Shell
    * Copyright 2020 Marcus Britanicus
	*

	*
	* This file is derived from QSingleApplication, originally written
	* as a part of Qt Solutions. For license read DesQ::Wayland::Application.cpp
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "WaylandApplication.hpp"
#include <QWidget>

#include "WlGlobal.hpp"
#include "Registry.hpp"

#include "DesQIpcServer.hpp"
#include "DesQIpcClient.hpp"

#include "DesQXdg.hpp"

static inline QString getSocketName( QString appId ) {

    QString sockName;

    /* $__DESQ_WORK_DIR is created and set by the session */
    if ( qgetenv( "__DESQ_WORK_DIR" ).count() )
        sockName = QString( qgetenv( "__DESQ_WORK_DIR" ) ) + "/" + appId;

    /* It does not exist: Let's create it */
    else {
        /* We're going to assume that XDG_RUNTIME_DIR exists and is writable at this point */
    	QString XDG_RUNTIME_DIR = DesQ::XDG::xdgRuntimeDir();

    	/* We're also going to assume that XDG_SESSION_ID exists */
    	QString XDG_SESSION_ID( qgetenv( "XDG_SESSION_ID" ) );

    	/* Create this path blindly. This MUST work: No fallbacks */
    	QDir( XDG_RUNTIME_DIR ).mkpath( "DesQSession-" + XDG_SESSION_ID + "/" );

        /* Our socket lives here */
        sockName = QDir( XDG_RUNTIME_DIR ).filePath( "DesQSession-" + XDG_SESSION_ID + "/" + appId );
    }

    return sockName;
};

DesQ::Wayland::Application::Application( const QString &appId, int &argc, char **argv ) : QApplication( argc, argv ) {

    /* App ID */
    mAppId = appId;

    /* Socket Name */
    mSocketName = getSocketName( appId );

    wl_display *display = DesQ::Wayland::getWlDisplay();
    if ( not display ) {
        qDebug() << "Unable to acquire wl_display from the compositor.";
        qDebug() << "Your experience will be severly limited.";

        registry = nullptr;
    }

    else {
        registry = new DesQ::Wayland::Registry( display );
        QObject::connect(
            registry, &DesQ::Wayland::Registry::errorOccured, [=]( DesQ::Wayland::Registry::ErrorType et ) {
                qDebug() << "Error caused on registry" << et;
                qDebug() << "Valiantly trying to continue...";
            }
        );

        registry->setup();
    }

    /* Lock File */
    lockFile = new QLockFile( mSocketName + ".lock" );

        /* Try to lock the @lockFile, if it fails, then we're not the first instance */
    if ( lockFile->tryLock() ) {
        /* IPC Server for communication */
        mIpcServer = new DesQIpcServer( this );

        /* Start the server */
        bool res = mIpcServer->listen( mSocketName );

        /* @res can't be false at the moment, because we're the first instance. */
        /* The only reason why @res is false, the socket file exists from a previous */
        /* crash. So delete it and try again. */
        if ( not res && mIpcServer->serverError() == QAbstractSocket::AddressInUseError ) {
            QLocalServer::removeServer( mSocketName );
            res = mIpcServer->listen( mSocketName );

            if ( !res )
                qWarning( "DesQ::Wayland::Application: listen on local socket failed, %s", qPrintable( mIpcServer->errorString() ) );
        }

        /* Irrespective of what happens, we will try to connect newConnection to receiveConnection */
        QObject::connect( mIpcServer, &DesQIpcServer::messageReceived, this, &DesQ::Wayland::Application::messageReceived );
    }
};

DesQ::Wayland::Application::~Application() {

    disconnect();

    if ( mIpcServer )
        mIpcServer->deleteLater();

    delete lockFile;
};

bool DesQ::Wayland::Application::isRunning() {

    /* If we have the lock, we're the server */
    /* In other words, if we're not there, there is no server */
    if ( lockFile->isLocked() )
        return false;

    /* If we cannot get the lock then the server is running elsewhere */
    if ( not lockFile->tryLock() )
        return true;

    /* Be default, we'll assume that the server is running elsewhere */
    return true;
};

QString DesQ::Wayland::Application::id() const {

    return mAppId;
};

DesQIpcServer* DesQ::Wayland::Application::ipcServer() {

    return mIpcServer;
};

bool DesQ::Wayland::Application::sendMessage( const QString &message ) {

    if ( not isRunning() )
        return false;

    /* Preparing socket */
    DesQIpcClient socket( DesQIpcClient::Message, this );

    /* Connecting to server */
    socket.connectToServer( mSocketName );

    /* Wait for ACK */
    if ( not socket.waitForAck() )
        return false;

    /* Send the message to the server */
    return socket.sendMessage( message.toUtf8() );
};

void DesQ::Wayland::Application::disconnect() {

    if ( mIpcServer )
        mIpcServer->close();

    lockFile->unlock();
};

void DesQ::Wayland::Application::setActivationWindow( QWidget* aw, bool activateOnMessage ) {

    /* Activation makes sense only when the server is active */
    if ( mIpcServer ) {
        actWin = aw;
        if ( activateOnMessage )
            QObject::connect( mIpcServer, &DesQIpcServer::messageReceived, this, &DesQ::Wayland::Application::activateWindow );

        else
            QObject::disconnect( mIpcServer, &DesQIpcServer::messageReceived, this, &DesQ::Wayland::Application::activateWindow );
    }
};

QWidget* DesQ::Wayland::Application::activationWindow() const {

    return actWin;
};

void DesQ::Wayland::Application::activateWindow() {

    if ( actWin ) {
        actWin->show();
        actWin->setWindowState( actWin->windowState() & ~Qt::WindowMinimized );
        actWin->raise();
        actWin->activateWindow();
    }
};

DesQ::Wayland::Registry* DesQ::Wayland::Application::waylandRegistry() {

    return registry;
};
