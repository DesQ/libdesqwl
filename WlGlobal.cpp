/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This file wa originally written as a part of PaperDE.
	* https://gitlab.com/cubocore/paperde)
	* Suitabe changes have been made to suit the needs of DesQ
	*

	*
	* This file was a part of IonShell (https://gitlab.com/cubocore/ion/ionde)
	* Suitable modifications have been done to meet the needs of DesQ.
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <QGuiApplication>
#include <QWindow>
#include <QScreen>

#include <qpa/qplatformnativeinterface.h>
#include <wayland-client.h>

#include "WlGlobal.hpp"

wl_display* DesQ::Wayland::getWlDisplay() {

	QPlatformNativeInterface *native = QGuiApplication::platformNativeInterface();
	if ( !native )
		return nullptr;

	struct wl_display *display = reinterpret_cast<wl_display *>( native->nativeResourceForWindow( "display", nullptr ) );

	return display;
};

wl_output* DesQ::Wayland::getWlOutput( QScreen *screen ) {

	QPlatformNativeInterface *native = QGuiApplication::platformNativeInterface();
	if ( !native )
		return nullptr;

	struct wl_output *output = reinterpret_cast<wl_output*>( native->nativeResourceForScreen( "output", screen ) );

	return output;
};

wl_surface* DesQ::Wayland::getWlSurface( QWindow *window ) {

	window->create();
	QPlatformNativeInterface *native = QGuiApplication::platformNativeInterface();
	if ( !native )
		return nullptr;

	struct wl_surface *surface = reinterpret_cast<wl_surface *>( native->nativeResourceForWindow( "surface", window ) );

	return surface;
};

bool DesQ::Wayland::isRunning() {

	/* Check if XDG_SESSION_TYPE is set */
	QString session = qgetenv( "XDG_SESSION_TYPE" );
	if ( session.toLower() == QStringLiteral( "wayland" ) )
		return true;

	/*
		* May be XDG_SESSION_TYPE is not set. Try Harder.
		* We check if WAYLAND_DISPLAY is set.
	*/
	QString wlID = qgetenv( "WAYLAND_DISPLAY" );
	if ( not wlID.isEmpty() )
		return true;

	/*
		* May be WAYLAND_DISPLAY is not set. Try Harder still.
		* We check if DESKTOP_SESSION is set.
	*/
	QString deSess = qgetenv( "DESKTOP_SESSION" );
	if ( deSess.contains( "wayland-sessions" ) )
		return true;

	/* It's probably not a wayland session. */
	return false;
};

bool DesQ::Wayland::isWayfire() {

	/** Checking the WAYFIRE_CONFIG_FILE variable is sufficient for our cause */
	QString wfConfigFile = qgetenv( "WAYFIRE_CONFIG_FILE" );
	if ( wfConfigFile.count() )
		return true;

	return false;
};
