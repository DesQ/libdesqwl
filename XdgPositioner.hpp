/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This file wa originally written as a part of PaperDE.
	* https://gitlab.com/cubocore/paperde)
	* Suitabe changes have been made to suit the needs of DesQ
	*

	*
	* This file was a part of IonShell (https://gitlab.com/cubocore/ion/ionde)
	* Suitable modifications have been done to meet the needs of DesQ.
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

class QWindow;
class QScreen;

struct wl_display;
struct wl_output;
struct wl_surface;

struct xdg_wm_base;

namespace DesQ::Wayland {
	class XdgPositioner;
};

class DesQ::Wayland::XdgPositioner {
	public:
	    /*
	     * Flags describing how a popup should be reposition if constrained
	     */
	    enum class Constraint {
			None = 0,
	        SlideX = 1 << 0,
	        SlideY = 1 << 1,
	        FlipX = 1 << 2,
	        FlipY = 1 << 3,
	        ResizeX = 1 << 4,
	        ResizeY = 1 << 5,
	    };
	    Q_DECLARE_FLAGS(Constraints, Constraint)

        enum Anchors {
            None = 0,
            Top = 1,
            Bottom = 2,
            Left = 3,
            Right = 4,
            TopLeft = 5,
            BottomLeft = 6,
            TopRight = 7,
            BottomRight = 8,
        };

        enum Gravity {
			None = 0,
            Top = 1,
            Bottom = 2,
            Left = 3,
            Right = 4,
            TopLeft = 5,
            BottomLeft = 6,
            TopRight = 7,
            BottomRight = 8,
        };

	    XdgPositioner( const QSize& initialSize = QSize(), const QRect& anchor = QRect() );
	    XdgPositioner( const XdgPositioner& other );
	    ~XdgPositioner();

	    Anchors anchorEdge() const;
	    void setAnchorEdge( Anchors edge );

	    Gravity gravity() const;
	    void setGravity( Gravity edge );

	    QRect anchorRect() const;
	    void setAnchorRect( const QRect& anchor );

	    QSize initialSize() const;
	    void setInitialSize( const QSize& size );

	    Constraints constraints() const;
	    void setConstraints( Constraints constraints );

	    QPoint anchorOffset() const;
	    void setAnchorOffset( const QPoint& offset );

		void setReactive();

	private:
		xdg_positioner *mObj;

		QSize mInitialSize;
		QRect mAnchorRect;
		Gravity mGravity;
		Anchors mAnchorEdge;
		Constraints mConstraints;
		QPoint mAnchorOffset;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(DesQ::Wayland::XdgPositioner::Constraints);

Q_DECLARE_METATYPE(DesQ::Wayland::XdgPositioner);
Q_DECLARE_METATYPE(DesQ::Wayland::XdgPositioner::Constraint);
Q_DECLARE_METATYPE(DesQ::Wayland::XdgPositioner::Constraints);
