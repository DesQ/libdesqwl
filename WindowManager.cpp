/*
	*
	* This file is a part of libpapershell-wl.
	* Library for handling various services related to Paper Shell.
	* Copyright 2020 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <QDebug>

#include "WindowManager.hpp"

#include "wayland-wlr-foreign-toplevel-management-unstable-v1-client-protocol.h"

DesQ::Wayland::WindowManager::WindowManager( zwlr_foreign_toplevel_manager_v1 *tlMgr ) {

	mObj = tlMgr;
	zwlr_foreign_toplevel_manager_v1_add_listener( mObj, &mWindowMgrListener, this );
};

DesQ::Wayland::WindowManager::~WindowManager() {

	zwlr_foreign_toplevel_manager_v1_destroy( mObj );
};

DesQ::Wayland::WindowHandles DesQ::Wayland::WindowManager::windowHandles() {

	return mTopLevels;
};

void DesQ::Wayland::WindowManager::handleTopLevelAdded( void *data, zwlr_foreign_toplevel_manager_v1 *, zwlr_foreign_toplevel_handle_v1 *hndl ) {

	WindowManager *winMgr = reinterpret_cast<WindowManager *>( data );

	DesQ::Wayland::WindowHandle *handle = new DesQ::Wayland::WindowHandle( hndl );
	winMgr->mTopLevels << handle;

	emit winMgr->newTopLevelHandle( handle );
};

void DesQ::Wayland::WindowManager::handleFinished( void *data, zwlr_foreign_toplevel_manager_v1 * ) {

	WindowManager *winMgr = reinterpret_cast<WindowManager *>( data );
	emit winMgr->finished();
};

const zwlr_foreign_toplevel_manager_v1_listener DesQ::Wayland::WindowManager::mWindowMgrListener = {
	handleTopLevelAdded,
	handleFinished,
};


/**
 * Window Handle Wrapper
 */

DesQ::Wayland::WindowHandle::WindowHandle( zwlr_foreign_toplevel_handle_v1 *hndl ) {

	if ( hndl ) {
		mObj = hndl;
		zwlr_foreign_toplevel_handle_v1_add_listener( mObj, &mWindowHandleListener, this );
	}
};

DesQ::Wayland::WindowHandle::~WindowHandle() {

	zwlr_foreign_toplevel_handle_v1_destroy( mObj );
};

QString DesQ::Wayland::WindowHandle::appId() const {

	return mAppId;
};

QString DesQ::Wayland::WindowHandle::title() const {

	return mTitle;
};

/** ======== Activated ======== */
bool DesQ::Wayland::WindowHandle::isActivated() {

	return mIsActivated;
};

void DesQ::Wayland::WindowHandle::activate( wl_seat *seat ) {

	zwlr_foreign_toplevel_handle_v1_activate( mObj, seat );
};

/** ======== Maximized ======== */
bool DesQ::Wayland::WindowHandle::isMaximized() {

	return mIsMaximized;
};

void DesQ::Wayland::WindowHandle::setMaximized() {

	zwlr_foreign_toplevel_handle_v1_set_maximized( mObj );
};

void DesQ::Wayland::WindowHandle::unsetMaximized() {

	zwlr_foreign_toplevel_handle_v1_unset_maximized( mObj );
};

/** ======== Minimized ======== */
bool DesQ::Wayland::WindowHandle::isMinimized() {

	return mIsMinimized;
};

void DesQ::Wayland::WindowHandle::setMinimized() {

	zwlr_foreign_toplevel_handle_v1_set_minimized( mObj );
};

void DesQ::Wayland::WindowHandle::unsetMinimized() {

	zwlr_foreign_toplevel_handle_v1_unset_minimized( mObj );
};

/** ======== FullScreen ======== */
bool DesQ::Wayland::WindowHandle::isFullScreen() {

	return mIsFullScreen;
};

void DesQ::Wayland::WindowHandle::setFullScreen() {

	zwlr_foreign_toplevel_handle_v1_set_fullscreen( mObj, nullptr );
};

void DesQ::Wayland::WindowHandle::unsetFullScreen() {

	zwlr_foreign_toplevel_handle_v1_unset_fullscreen( mObj );
};

void DesQ::Wayland::WindowHandle::close() {

	zwlr_foreign_toplevel_handle_v1_close( mObj );
};

void DesQ::Wayland::WindowHandle::handleTitle( void *data, zwlr_foreign_toplevel_handle_v1 *hndl, const char *title ) {

	WindowHandle *handle = reinterpret_cast<WindowHandle *>( data );
	handle->mTitle = title;

	emit handle->titleChanged();
};

void DesQ::Wayland::WindowHandle::handleAppId( void *data, zwlr_foreign_toplevel_handle_v1 *hndl, const char *app_id ) {

	WindowHandle *handle = reinterpret_cast<WindowHandle *>( data );
	handle->mAppId = app_id;

	emit handle->appIdChanged();
};

void DesQ::Wayland::WindowHandle::handleOutputEnter( void *data, zwlr_foreign_toplevel_handle_v1 *hndl, wl_output *wlOut ) {

	WindowHandle *handle = reinterpret_cast<WindowHandle *>( data );
	emit handle->outputEntered( wlOut );
};

void DesQ::Wayland::WindowHandle::handleOutputLeave( void *data, zwlr_foreign_toplevel_handle_v1 *hndl, wl_output *wlOut ) {

	WindowHandle *handle = reinterpret_cast<WindowHandle *>( data );
	emit handle->outputLeft( wlOut );
};

void DesQ::Wayland::WindowHandle::handleState( void *data, zwlr_foreign_toplevel_handle_v1 *, wl_array *state ) {

	auto *states = static_cast<uint32_t *>( state->data );
	int numStates = static_cast<int>( state->size / sizeof( uint32_t ) );

	WindowHandle *handle = reinterpret_cast<WindowHandle *>( data );
	handle->mIsMaximized = false;
	handle->mIsMinimized = false;
	handle->mIsActivated = false;
	handle->mIsFullScreen = false;

	for ( int i = 0; i < numStates; i++ ) {
		switch( (uint32_t)states[ i ] ) {
			case ZWLR_FOREIGN_TOPLEVEL_HANDLE_V1_STATE_MAXIMIZED: {
				handle->mIsMaximized = true;
				break;
			}

			case ZWLR_FOREIGN_TOPLEVEL_HANDLE_V1_STATE_MINIMIZED: {
				handle->mIsMinimized = true;
				break;
			}

			case ZWLR_FOREIGN_TOPLEVEL_HANDLE_V1_STATE_ACTIVATED: {
				handle->mIsActivated = true;
				break;
			}

			case ZWLR_FOREIGN_TOPLEVEL_HANDLE_V1_STATE_FULLSCREEN: {
				handle->mIsFullScreen = true;
				break;
			}
		}
	}

	emit handle->stateChanged();
};

void DesQ::Wayland::WindowHandle::handleDone( void *data, zwlr_foreign_toplevel_handle_v1 * ) {

	WindowHandle *handle = reinterpret_cast<WindowHandle *>( data );
	emit handle->done();
};

void DesQ::Wayland::WindowHandle::handleClosed( void *data, zwlr_foreign_toplevel_handle_v1 * ) {

	WindowHandle *handle = reinterpret_cast<WindowHandle *>( data );
	emit handle->closed();
};

void DesQ::Wayland::WindowHandle::handleParent( void *data, zwlr_foreign_toplevel_handle_v1 *, zwlr_foreign_toplevel_handle_v1 *parent) {

	WindowHandle *handle = reinterpret_cast<WindowHandle *>( data );

	if ( parent ) {
		emit handle->parentChanged( new WindowHandle( parent ) );
		qDebug() << handle << "parent changed";
	}
};

const zwlr_foreign_toplevel_handle_v1_listener DesQ::Wayland::WindowHandle::mWindowHandleListener = {
	handleTitle,
	handleAppId,
	handleOutputEnter,
	handleOutputLeave,
	handleState,
	handleDone,
	handleClosed,
	handleParent,
};
