/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This file wa originally written as a part of PaperDE.
	* https://gitlab.com/cubocore/paperde)
	* Suitabe changes have been made to suit the needs of DesQ
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include <QObject>
#include <QMargins>
#include <QSize>

class QWindow;

struct zwlr_layer_shell_v1;
struct zwlr_layer_surface_v1;
struct zwlr_layer_surface_v1_listener;
struct wl_output;

namespace DesQ {
    namespace Wayland {
		class LayerShell;
		class LayerSurface;
	};
};

class DesQ::Wayland::LayerShell : public QObject {
	Q_OBJECT;

	public:
		enum LayerType {
			Background = 0,
			Bottom = 1,
			Top = 2,
			Overlay = 3
		};

		LayerShell( zwlr_layer_shell_v1 *lShell );
		~LayerShell();

		LayerSurface *getLayerSurface( QWindow *window, wl_output *output, LayerType layer, const QString &lyrNs );

		operator zwlr_layer_shell_v1 *();
		operator zwlr_layer_shell_v1 *() const;

	private:
		zwlr_layer_shell_v1 *mObj;
};

class DesQ::Wayland::LayerSurface : public QObject {
	Q_OBJECT;

	public:
		enum SurfaceAnchor {
			NoAchor = 0,
			Top = (1 << 0),
			Bottom = (1 << 1),
			Left = (1 << 2),
			Right = (1 << 3)
		};
		Q_DECLARE_FLAGS(SurfaceAnchors, SurfaceAnchor);

		/* Keyboard focus */
		enum FocusType {
			NoFocus = 0,
			Exclusive = 1,
			OnDemand = 2
		};

		LayerSurface( QWindow *window, zwlr_layer_surface_v1 *lyrSurf );
		~LayerSurface();

		void apply();

		void setSurfaceSize( const QSize &surfaceSize );
		void setAnchors( const SurfaceAnchors &anchors );
		void setExclusiveZone( int exclusiveZone );
		void setMargins( const QMargins &margins );
		void setKeyboardInteractivity( FocusType focusType );
		void setLayer( DesQ::Wayland::LayerShell::LayerType type );

		void getPopup( QWindow *popupWin );

		operator zwlr_layer_surface_v1*();
		operator zwlr_layer_surface_v1*() const;

	private:
		QWindow *mWindow;
		zwlr_layer_surface_v1 *mObj;

		QSize m_surfaceSize{ 0, 0 };
		int m_exclusiveZone = -1;
		QMargins m_margins{ 0, 0, 0, 0 };
		uint m_keyboardInteractivity = 2;

		SurfaceAnchors m_anchors;
		DesQ::Wayland::LayerShell::LayerType m_lyrType;

		void configureSurface( uint32_t serial, uint32_t width, uint32_t height );
		void closeSurface();

		static void configureCallback( void *data, zwlr_layer_surface_v1 *object, uint32_t serial, uint32_t width, uint32_t height );
		static void closedCallback( void *data, zwlr_layer_surface_v1 *object );

		static const zwlr_layer_surface_v1_listener mLyrSurfListener;
};

Q_DECLARE_OPERATORS_FOR_FLAGS( DesQ::Wayland::LayerSurface::SurfaceAnchors );
