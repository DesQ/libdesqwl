/*
	*
	* This file is a part of libpaper.
	* Library for handling various services related to Paper Shell.
	* Copyright 2020 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include <QObject>

struct wl_seat;
struct wl_array;
struct wl_output;
struct zwlr_foreign_toplevel_handle_v1;
struct zwlr_foreign_toplevel_handle_v1_listener;
struct zwlr_foreign_toplevel_manager_v1;
struct zwlr_foreign_toplevel_manager_v1_listener;

namespace DesQ {
	namespace Wayland {
		class WindowManager;
		class WindowHandle;
		typedef QList<WindowHandle *> WindowHandles;
	};
};

class DesQ::Wayland::WindowManager : public QObject {
	Q_OBJECT;

	public:
		WindowManager( zwlr_foreign_toplevel_manager_v1 *winMgr );
		~WindowManager();

		operator zwlr_foreign_toplevel_manager_v1 *();
		operator zwlr_foreign_toplevel_manager_v1 *() const;

		DesQ::Wayland::WindowHandles windowHandles();

	private:
		/**
		 * Get the handle, and send out a signal.
		 */
		static void handleTopLevelAdded( void *, zwlr_foreign_toplevel_manager_v1 *, zwlr_foreign_toplevel_handle_v1 * );
		static void handleFinished( void *, zwlr_foreign_toplevel_manager_v1 * );

		static const zwlr_foreign_toplevel_manager_v1_listener mWindowMgrListener;

		zwlr_foreign_toplevel_manager_v1 *mObj;
		DesQ::Wayland::WindowHandles mTopLevels;

	Q_SIGNALS:
		/**
		 * Get the handle
		 */
		void newTopLevelHandle( DesQ::Wayland::WindowHandle *handle );
		void finished();
};

class DesQ::Wayland::WindowHandle : public QObject {
	Q_OBJECT;

	public:
		WindowHandle( zwlr_foreign_toplevel_handle_v1 *handle );
		~WindowHandle();

		QString appId() const;
		QString title() const;

		bool isActivated();
		void activate( wl_seat *seat );

		bool isMaximized();
		void setMaximized();
		void unsetMaximized();

		bool isMinimized();
		void setMinimized();
		void unsetMinimized();

		bool isFullScreen();
		void setFullScreen();
		void unsetFullScreen();

		void close();

	private:
		static void handleTitle( void *data, zwlr_foreign_toplevel_handle_v1 *hndl, const char *title );
		static void handleAppId( void *data, zwlr_foreign_toplevel_handle_v1 *hndl, const char *app_id );
		static void handleOutputEnter( void *data, zwlr_foreign_toplevel_handle_v1 *hndl, wl_output *wlOut );
		static void handleOutputLeave( void *data, zwlr_foreign_toplevel_handle_v1 *hndl, wl_output *wlOut );
		static void handleState( void *data, zwlr_foreign_toplevel_handle_v1 *hndl, wl_array *state );
		static void handleDone( void *data, zwlr_foreign_toplevel_handle_v1 *hndl );
		static void handleClosed( void *data, zwlr_foreign_toplevel_handle_v1 *hndl );
		static void handleParent( void *data, zwlr_foreign_toplevel_handle_v1 *hndl, zwlr_foreign_toplevel_handle_v1 *parent);

		static const zwlr_foreign_toplevel_handle_v1_listener mWindowHandleListener;

		zwlr_foreign_toplevel_handle_v1 *mObj;

		QString mTitle;
		QString mAppId;

		bool mIsActivated = false;
		bool mIsMaximized = false;
		bool mIsMinimized = false;
		bool mIsFullScreen = false;

	Q_SIGNALS:
		void titleChanged();
		void appIdChanged();
		void outputEntered( wl_output *wlOut );
		void outputLeft( wl_output *wlOut );
		void stateChanged();
		void done();
		void closed();
		void parentChanged( WindowHandle *parent );
};
