/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This file wa originally written as a part of PaperDE.
	* https://gitlab.com/cubocore/paperde)
	* Suitabe changes have been made to suit the needs of DesQ
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <QDebug>
#include <QWindow>

#include "wayland-wayfire-shell-unstable-v2-client-protocol.h"

#include "LayerShell.hpp"
#include "WlGlobal.hpp"
#include "WayfireShell.hpp"

DesQ::Wayland::WayfireShell::WayfireShell( zwf_shell_manager_v2 *shellMgr ) {

	mObj = shellMgr;
};

DesQ::Wayland::WayfireShell::~WayfireShell() {

	zwf_shell_manager_v2_destroy( mObj );
};

DesQ::Wayland::WayfireOutput* DesQ::Wayland::WayfireShell::getOutput( wl_output *wlOut ) {

	/**
	 * We get the wfOut object. Now we need to send this to WayfireOutput*
	 * and hook it up to a listener.
	 */
	zwf_output_v2 *wfOut = zwf_shell_manager_v2_get_wf_output( mObj, wlOut );

	return new DesQ::Wayland::WayfireOutput( wfOut );
};

DesQ::Wayland::WayfireShell::operator zwf_shell_manager_v2 *() {

	return mObj;
};

DesQ::Wayland::WayfireShell::operator zwf_shell_manager_v2 *() const {

	return mObj;
};

/**
 * WayfireOutput Wrapper
 */

DesQ::Wayland::WayfireOutput::WayfireOutput( zwf_output_v2 *wfOut ) {

	mObj = wfOut;
};

DesQ::Wayland::WayfireOutput::~WayfireOutput() {

	zwf_output_v2_destroy( mObj );
};

void DesQ::Wayland::WayfireOutput::setup() {

	zwf_output_v2_add_listener( mObj, &mWfOutputListener, this );
};

DesQ::Wayland::WayfireHotSpot* DesQ::Wayland::WayfireOutput::createHotSpot( HotSpot hsPos, uint32_t threshold, uint32_t timeout ) {

	zwf_hotspot_v2 *wfHS = zwf_output_v2_create_hotspot( mObj, hsPos, threshold, timeout );
	return new DesQ::Wayland::WayfireHotSpot( wfHS );
};

DesQ::Wayland::WayfireOutput::operator zwf_output_v2 *() {

	return mObj;
};

DesQ::Wayland::WayfireOutput::operator zwf_output_v2 *() const {

	return mObj;
};

void DesQ::Wayland::WayfireOutput::handleEnterFullScreen( void *data, zwf_output_v2 *wfOutput ) {

	Q_UNUSED( wfOutput );

	WayfireOutput *wfOpWrap = reinterpret_cast<WayfireOutput *>( data );
	emit wfOpWrap->enteredFullScreen();
};

void DesQ::Wayland::WayfireOutput::handleLeaveFullScreen( void *data, zwf_output_v2 *wfOutput ) {

	Q_UNUSED( wfOutput );

	WayfireOutput *wfOpWrap = reinterpret_cast<WayfireOutput *>( data );
	emit wfOpWrap->leftFullScreen();
};

const struct zwf_output_v2_listener DesQ::Wayland::WayfireOutput::mWfOutputListener = {
	handleEnterFullScreen,
	handleLeaveFullScreen,
};

/**
 * WayfireHotSpot Wrapper
 */

DesQ::Wayland::WayfireHotSpot::WayfireHotSpot( zwf_hotspot_v2 *wfHS ) {

	mObj = wfHS;
};

DesQ::Wayland::WayfireHotSpot::~WayfireHotSpot() {

	zwf_hotspot_v2_destroy( mObj );
};

void DesQ::Wayland::WayfireHotSpot::setup() {

	zwf_hotspot_v2_add_listener( mObj, &mWfHotSpotListener, this );
};

DesQ::Wayland::WayfireHotSpot::operator zwf_hotspot_v2 *() {

	return mObj;
};

DesQ::Wayland::WayfireHotSpot::operator zwf_hotspot_v2 *() const {

	return mObj;
};

void DesQ::Wayland::WayfireHotSpot::handleEnterHotSpot( void *data, zwf_hotspot_v2 *wfHotspot ) {

	Q_UNUSED( wfHotspot );

	WayfireHotSpot *wfHsWrap = reinterpret_cast<WayfireHotSpot *>( data );
	emit wfHsWrap->enteredHotSpot();
};

void DesQ::Wayland::WayfireHotSpot::handleLeaveHotSpot( void *data, zwf_hotspot_v2 *wfHotspot ) {

	Q_UNUSED( wfHotspot );

	WayfireHotSpot *wfHsWrap = reinterpret_cast<WayfireHotSpot *>( data );
	emit wfHsWrap->leftHotSpot();
};

const struct zwf_hotspot_v2_listener DesQ::Wayland::WayfireHotSpot::mWfHotSpotListener = {
	handleEnterHotSpot,
	handleLeaveHotSpot,
};
