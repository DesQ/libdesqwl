/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This file wa originally written as a part of PaperDE.
	* https://gitlab.com/cubocore/paperde)
	* Suitabe changes have been made to suit the needs of DesQ
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include <QObject>
#include <QTimer>

class QWindow;
class QScreen;
class DesQLayerSurface;
class WayfireHotSpot;
class WayfireHotSpotPrivate;

struct wl_output;
struct zwf_shell_manager_v2;
struct zwf_output_v2;
struct zwf_output_v2_listener;
struct zwf_hotspot_v2;
struct zwf_hotspot_v2_listener;

namespace DesQ {
    namespace Wayland {
		class WayfireShell;
		class WayfireOutput;
		class WayfireHotSpot;
	}
};

class DesQ::Wayland::WayfireShell : public QObject {
	Q_OBJECT;

	public:
		WayfireShell( zwf_shell_manager_v2 *shellMgr );
		~WayfireShell();

		/**
		 * Get the wayfire output for the give wl_output.
		 * Don't forget to call WayfireOutput::setup() before using it
		 */
		DesQ::Wayland::WayfireOutput* getOutput( wl_output * );

		operator zwf_shell_manager_v2 *();
		operator zwf_shell_manager_v2 *() const;

	private:
		zwf_shell_manager_v2 *mObj;
};

class DesQ::Wayland::WayfireOutput : public QObject {
	Q_OBJECT;

	public:
		enum HotSpot {
			Top    = 1,
			Bottom = 2,
			Left   = 4,
			Right  = 8,
			TopLeft     = Top | Left,
			TopRight    = Top | Right,
			BottomLeft  = Bottom | Left,
			BottomRight = Bottom | Right
		};

		Q_ENUM( HotSpot );

		WayfireOutput( zwf_output_v2 *wfOutput );
		~WayfireOutput();

		/**
		 * We would like to give others a chance to setup signals
		 */
		void setup();

		/**
		 * Create a hotspot for a @location, defined by a region @threshold px
		 * and the mouse stays there for atleast @timeout ms.
		 */
		DesQ::Wayland::WayfireHotSpot* createHotSpot( HotSpot location, uint32_t threshold, uint32_t timeout );

		operator zwf_output_v2 *();
		operator zwf_output_v2 *() const;

	private:
		zwf_output_v2 *mObj;

		static void handleEnterFullScreen( void *data, zwf_output_v2 *wfOut );
		static void handleLeaveFullScreen( void *data, zwf_output_v2 *wfOut );
		static const zwf_output_v2_listener mWfOutputListener;

	Q_SIGNALS:
		void enteredFullScreen();
		void leftFullScreen();
};

class DesQ::Wayland::WayfireHotSpot : public QObject {
	Q_OBJECT;

	public:
		WayfireHotSpot( zwf_hotspot_v2* );
		~WayfireHotSpot();

		void setup();

		operator zwf_hotspot_v2 *();
		operator zwf_hotspot_v2 *() const;

	private:
		zwf_hotspot_v2 *mObj;

		static void handleEnterHotSpot( void *data, zwf_hotspot_v2 *wfHotspot );
		static void handleLeaveHotSpot( void *data, zwf_hotspot_v2 *wfHotspot );
		static const zwf_hotspot_v2_listener mWfHotSpotListener;

	Q_SIGNALS:
		void enteredHotSpot();
		void leftHotSpot();
};
