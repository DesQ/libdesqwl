/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This file wa originally written as a part of PaperDE.
	* https://gitlab.com/cubocore/paperde)
	* Suitabe changes have been made to suit the needs of DesQ
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include <QObject>
#include <QMargins>
#include <QSize>

struct zwlr_input_inhibit_manager_v1;
struct zwlr_input_inhibitor_v1;

namespace DesQ {
    namespace Wayland {
		class InputInhibitManager;
		class InputInhibitor;
	};
};

class DesQ::Wayland::InputInhibitManager : public QObject {
	Q_OBJECT;

	public:
		InputInhibitManager( zwlr_input_inhibit_manager_v1 *iimgr );
		~InputInhibitManager();

		DesQ::Wayland::InputInhibitor* getInputInhibitor();

		operator zwlr_input_inhibit_manager_v1 *();
		operator zwlr_input_inhibit_manager_v1 *() const;

	private:
		zwlr_input_inhibit_manager_v1 *mObj;
};

class DesQ::Wayland::InputInhibitor : public QObject {
	Q_OBJECT;

	public:
		InputInhibitor( zwlr_input_inhibitor_v1 *inhibitor );
		~InputInhibitor();

		void destroy();

		operator zwlr_input_inhibitor_v1 *();
		operator zwlr_input_inhibitor_v1 *() const;

	private:
		zwlr_input_inhibitor_v1 *mObj;
};
