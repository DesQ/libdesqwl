/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This file wa originally written as a part of PaperDE.
	* https://gitlab.com/cubocore/paperde)
	* Suitabe changes have been made to suit the needs of DesQ
	*

	*
	* This file was a part of IonShell (https://gitlab.com/cubocore/ion/ionde)
	* Suitable modifications have been done to meet the needs of DesQ.
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <QDebug>

#include "Registry.hpp"
#include "LayerShell.hpp"
#include "WayfireShell.hpp"
#include "WindowManager.hpp"
#include "InputInhibition.hpp"

#include "wayland-wayland-client-protocol.h"
#include "wayland-wlr-layer-shell-unstable-v1-client-protocol.h"
#include "wayland-wayfire-shell-unstable-v2-client-protocol.h"
#include "wayland-wlr-input-inhibit-unstable-v1-client-protocol.h"
#include "wayland-wlr-foreign-toplevel-management-unstable-v1-client-protocol.h"

/* Convenience functions */
void DesQ::Wayland::Registry::globalAnnounce( void* data, struct wl_registry*, uint32_t name, const char* interface, uint32_t version ) {

	auto r = reinterpret_cast<DesQ::Wayland::Registry*>( data );
	r->handleAnnounce( name, interface, version );
};

void DesQ::Wayland::Registry::globalRemove( void* data, struct wl_registry* registry, uint32_t name ) {

	// who cares :D
	// but we will call DesQ::Wayland::Registry::handleRemove just for the heck of it

	auto r = reinterpret_cast<DesQ::Wayland::Registry*>( data );
	r->handleRemove( name );
};

const struct wl_registry_listener DesQ::Wayland::Registry::mRegListener = {
    globalAnnounce,
    globalRemove,
};

DesQ::Wayland::Registry::Registry( wl_display *wlDisplay ) {

	mWlDisplay = wlDisplay;
	mObj = wl_display_get_registry( mWlDisplay );

	mWlrLayerShell = nullptr;
	mLayerShell = nullptr;

	mWfShellMgr = nullptr;
	mWayfireShell = nullptr;

	mWlrInhibitMgr = nullptr;
	mInhibitManager = nullptr;

	mWlrWindowMgr = nullptr;
	mWindowMgr = nullptr;

	mWlSeat = nullptr;
};

DesQ::Wayland::Registry::~Registry() {

	wl_registry_destroy( mObj );
};

void DesQ::Wayland::Registry::setup() {

	wl_registry_add_listener( mObj, &mRegListener, this );
	wl_display_roundtrip( mWlDisplay );
};

DesQ::Wayland::Registry::operator wl_registry*() const {

    return mObj;
}

DesQ::Wayland::Registry::operator wl_registry*() {

	return mObj;
}

wl_display* DesQ::Wayland::Registry::waylandDisplay() {

	return mWlDisplay;
};

wl_seat* DesQ::Wayland::Registry::waylandSeat() {

	return mWlSeat;
};

DesQ::Wayland::LayerShell* DesQ::Wayland::Registry::layerShell() {

	return mLayerShell;
};

DesQ::Wayland::WayfireShell* DesQ::Wayland::Registry::wayfireShell() {

	return mWayfireShell;
};

DesQ::Wayland::WindowManager* DesQ::Wayland::Registry::windowManager() {

	return mWindowMgr;
};

DesQ::Wayland::InputInhibitManager* DesQ::Wayland::Registry::inputInhibitManager() {

	return mInhibitManager;
};

void DesQ::Wayland::Registry::handleAnnounce( uint32_t name, const char *interface, uint32_t version ) {

	if ( strcmp( interface, wl_seat_interface.name ) == 0 ) {
		mWlSeat = (wl_seat*)wl_registry_bind( mObj, name, &wl_seat_interface, version );

		if (!mWlSeat)
			emit errorOccured( DesQ::Wayland::Registry::EmptySeat );
	}

	else if ( strcmp( interface, zwlr_layer_shell_v1_interface.name ) == 0 ) {
		mWlrLayerShell = (zwlr_layer_shell_v1*)wl_registry_bind( mObj, name, &zwlr_layer_shell_v1_interface, 4 );

		if ( !mWlrLayerShell )
			emit errorOccured( DesQ::Wayland::Registry::EmptyLayerShell );

		else
			mLayerShell = new DesQ::Wayland::LayerShell( mWlrLayerShell );
	}

	else if ( strcmp( interface, zwlr_input_inhibit_manager_v1_interface.name ) == 0 ) {
		mWlrInhibitMgr = (zwlr_input_inhibit_manager_v1*)wl_registry_bind( mObj, name, &zwlr_input_inhibit_manager_v1_interface, 1 );

		if ( !mWlrInhibitMgr )
			emit errorOccured( DesQ::Wayland::Registry::EmptyInputInhibitManager );

		else
			mInhibitManager = new DesQ::Wayland::InputInhibitManager( mWlrInhibitMgr );
	}

	else if ( strcmp( interface, zwlr_foreign_toplevel_manager_v1_interface.name ) == 0 ) {
		mWlrWindowMgr = (zwlr_foreign_toplevel_manager_v1*)wl_registry_bind( mObj, name, &zwlr_foreign_toplevel_manager_v1_interface, 3 );

		if ( !mWlrWindowMgr )
			emit errorOccured( DesQ::Wayland::Registry::EmptyToplevelManager );

		else
			mWindowMgr = new DesQ::Wayland::WindowManager( mWlrWindowMgr );
	}

	else if ( strcmp( interface, zwf_shell_manager_v2_interface.name ) == 0 ) {
		mWfShellMgr = (zwf_shell_manager_v2*)wl_registry_bind( mObj, name, &zwf_shell_manager_v2_interface, 1 );

		if ( !mWfShellMgr )
			emit errorOccured( DesQ::Wayland::Registry::EmptyWayfireShell );

		else
			mWayfireShell = new DesQ::Wayland::WayfireShell( mWfShellMgr );
	}
};

void DesQ::Wayland::Registry::handleRemove( uint32_t /* name */ ) {
	// who cares :D
	// Or may be we should call *_destroy(...) here?
};
