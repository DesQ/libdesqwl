/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This file wa originally written as a part of PaperDE.
	* https://gitlab.com/cubocore/paperde)
	* Suitabe changes have been made to suit the needs of DesQ
	*

	*
	* This file was a part of IonShell (https://gitlab.com/cubocore/ion/ionde)
	* Suitable modifications have been done to meet the needs of DesQ.
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

class QWindow;
class QScreen;

struct wl_display;
struct wl_output;
struct wl_surface;

struct xdg_wm_base;

namespace DesQ::Wayland {
	class XdgShell;
};

class DesQ::Wayland::XdgShell : public QObject {
    Q_OBJECT;

	public:
        XdgShell( QObject* parent = nullptr );
        ~XdgShell();

        void setup(xdg_wm_base* xdg_wm_base);

        XdgShellToplevel* create_toplevel( Surface* surface, QObject* parent = nullptr );

        XdgShellPopup* create_popup(
            Surface* surface,
            XdgShellToplevel* parentSurface,
            XdgPositioner const& positioner,
            QObject* parent = nullptr
        );

        XdgShellPopup* create_popup(
            Surface* surface,
            XdgShellPopup* parentSurface,
            XdgPositioner const& positioner,
            QObject* parent = nullptr
        );

        XdgShellPopup* create_popup(
            Surface* surface,
            XdgPositioner const& positioner,
            QObject* parent = nullptr
        );

        operator xdg_wm_base*();
        operator xdg_wm_base*() const;

    Q_SIGNALS:
        void removed();

    private:
        xdg_wm_base *mObj;
};
