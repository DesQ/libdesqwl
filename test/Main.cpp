#include <QtWidgets>
#include <iostream>

#include "DesQWlGlobal.hpp"
#include "DesQRegistry.hpp"
#include "DesQWindowManager.hpp"

namespace DWL = DesQ::Wayland;

class TestDrive: public QObject {
    Q_OBJECT;

    public:
        TestDrive( DWL::Registry *reg ) {
            winMgr = reg->windowManager();
            connect(
                winMgr, &DWL::WindowManager::newTopLevelHandle, [=]( DesQ::Wayland::WindowHandle *handle ) mutable {
                    qDebug() << "Handle Added" << handle;
                    topLevels << handle;

                    connect( handle, &DesQ::Wayland::WindowHandle::appIdChanged, [=]() {
                        qDebug() << "(appid)" << handle->appId() << handle->title();
                    } );

                    connect( handle, &DesQ::Wayland::WindowHandle::titleChanged, [=]() {
                        qDebug() << "(title)" << handle->appId() << handle->title();
                    } );
                }
            );
        };


    private:
        DWL::WindowManager *winMgr;
        DWL::WindowHandles topLevels;
};

int main( int argc, char *argv[] ) {

    qputenv( "QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral( "1" ) );
    QApplication app( argc, argv );

    // QToolButton *w = new QToolButton();
    // w->setIcon( QIcon::fromTheme( "folder" ) );
    // w->setIconSize( QSize( 64, 64 ) );
    // w->setFixedSize( QSize( 72, 72 ) );
    QTextEdit *w = new QTextEdit();
    w->setFixedSize( QSize( 800, 600 ) );
    w->setWindowFlags( Qt::BypassWindowManagerHint | Qt::FramelessWindowHint );

    DWL::Registry *reg = new DWL::Registry( DWL::getWlDisplay() );
    reg->setup();

    TestDrive drv( reg );

    return app.exec();
};

#include "Main.moc"
