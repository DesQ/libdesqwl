/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This file was a part of PaperDE (https://gitlab.com/cubocore/paper/paperde)
	* Suitable modifications have been done to meet the needs of DesQ.
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include <QObject>

struct wl_registry;
struct wl_display;
struct wl_seat;
// struct wl_compositor;

struct wl_registry_listener;

// struct xdg_wm_base;
struct zwlr_layer_shell_v1;
struct zwf_shell_manager_v2;
struct zwlr_foreign_toplevel_manager_v1;
struct zwlr_input_inhibit_manager_v1;

namespace DesQ {
    namespace Wayland {
		class Registry;
		class LayerShell;
		class LayerSurface;
		class WayfireShell;
		class WindowManager;
		class InputInhibitManager;
	};
};

class DesQ::Wayland::Registry : public QObject {
	Q_OBJECT;

	public:
		enum ErrorType {
			EmptyXdgWmBase,
			EmptyCompositor,
			EmptySeat,
			EmptyLayerShell,
			EmptyToplevelManager,
			EmptyInputInhibitManager,
			EmptyWayfireShell
		};
		Q_ENUM(ErrorType);

		Registry( wl_display *wlDisplay );
		~Registry();

		void setup();

		operator wl_registry*();
		operator wl_registry*() const;

        wl_display* waylandDisplay();
        wl_seat* waylandSeat();

		/* Ready to use Wayland Classes */

		/**
		 * LayerShell - Wlr Layer Shell protocol implementation
		 */
		DesQ::Wayland::LayerShell *layerShell();

		/**
		 * WayfireShell - Wayfire Shell protocol implementation
		 */
		DesQ::Wayland::WayfireShell *wayfireShell();

		/**
		 * InputInhibitManager - Wlr Input Inhibitor protocol implementation
		 */
		DesQ::Wayland::InputInhibitManager *inputInhibitManager();

		/**
		 * WindowManager - Wlr TopLevel Manager protocol implementation
		 */
		DesQ::Wayland::WindowManager *windowManager();

	private:
        /** Raw C pointer to this class */
		wl_registry *mObj;

        /** wl_display object */
		wl_display *mWlDisplay;

        /** wl_seat object */
		wl_seat *mWlSeat;

		/**
		 * Layer Shell Objects
		 */
		zwlr_layer_shell_v1 *mWlrLayerShell;
		DesQ::Wayland::LayerShell *mLayerShell;

		/**
		 * Wayfire Shell Objects
		 */
		zwf_shell_manager_v2 *mWfShellMgr;
		DesQ::Wayland::WayfireShell *mWayfireShell;

		/**
		 * Wayfire Shell Objects
		 */
		zwlr_input_inhibit_manager_v1 *mWlrInhibitMgr;
		DesQ::Wayland::InputInhibitManager *mInhibitManager;

		/**
		 * TopLevel Objects
		 */
		zwlr_foreign_toplevel_manager_v1 *mWlrWindowMgr;
		DesQ::Wayland::WindowManager *mWindowMgr;

		static const wl_registry_listener mRegListener;
		static void globalAnnounce( void* data, wl_registry* registry, uint32_t name, const char* interface, uint32_t version );
		static void globalRemove( void* data, wl_registry* registry, uint32_t name );

		void handleAnnounce( uint32_t name, const char* interface, uint32_t version );
		void handleRemove( uint32_t name );

	signals:
		void errorOccured( ErrorType et );
};
